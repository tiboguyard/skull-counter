import { StyleSheet } from "react-native";
import { COLORS_DARK } from "../constantes/theme/dark.theme";

const TableStyle = StyleSheet.create({
    table: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        borderColor: COLORS_DARK.black_40,
        borderWidth: 2,
        borderRadius: 7
    },
    column: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    row: { display: 'flex', alignItems: 'stretch' },
    columnName: {
        color: COLORS_DARK.black_40,
        borderColor: COLORS_DARK.black_40
    },
    rowData: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        color: COLORS_DARK.white,
        borderColor: COLORS_DARK.black_40,
        borderTopWidth: 2,
        borderRightWidth: 2,
        padding: 7
    },
    lastRowData: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        color: COLORS_DARK.white,
        borderColor: COLORS_DARK.black_40,
        borderTopWidth: 2,
        padding: 7
    }
});

export {
    TableStyle
};


import { StyleSheet } from "react-native";

const ModalStyles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-start',
        borderWidth: 2,
        padding: 20,
        borderRadius: 10,
        gap: 15
    },
    header: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'baseline',
        justifyContent: 'space-between',
        width: '100%'
    },
    headerLeft: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'baseline',
        gap: 10
    },
    headerTitle: {
        fontSize: 20
    },
    headerSubTitle: {
        fontSize: 16
    },
    headerInfo: {
        fontSize: 9
    },
    inputBorder: {
        borderWidth: 1,
        width: '100%',
        borderRadius: 10,
        paddingHorizontal: 10
    },
    inputWithoutBorder: {
        width: 'auto',
        borderRadius: 10,
        paddingHorizontal: 10
    },
    bottom: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        width: '100%'
    }
});

export {
    ModalStyles
};


import { StyleSheet } from "react-native";

const NavigationStyle = StyleSheet.create({
    padding: {
        paddingHorizontal: 10,
        paddingVertical: 35
    },
    container: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'baseline',
        justifyContent: 'space-between',
        borderRadius: 15,
        paddingHorizontal: 20,
        paddingVertical: 10
    },
    left: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'baseline',
        gap: 10
    },
    title: {
        fontSize: 20
    },
    subTitle: {
        fontSize: 16
    },
    subInfo: {
        fontSize: 9
    }

});

export {
    NavigationStyle
};


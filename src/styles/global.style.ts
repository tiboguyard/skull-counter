import { StyleSheet } from "react-native";

const GlobalStyles = StyleSheet.create({
    viewHolder: {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        gap: 15,
        paddingHorizontal: 10
    },
    roundedButton: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        width: 60,
        height: 60,
        borderRadius: 90
    },
    cornerRoundedButton: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        width: 'auto',
        height: 'auto',
        paddingHorizontal: 20,
        paddingVertical: 10,
        borderWidth: 2,
        borderRadius: 20
    },
    floatRightContainer: {
        position: 'absolute',
        bottom: 20 + 80,
        right: 20,
        display: 'flex',
        flexDirection: 'column',
        gap: 10
    },
    floatCenterContainer: {
        position: 'absolute',
        bottom: 80 - 45,
        zIndex: 100,
        display: 'flex',
        alignItems: 'center',
        left: '50%',
        transform: [{ translateX: -50 }]
    },
    floatLeftContainer: {
        position: 'absolute',
        bottom: 20 + 80,
        left: 20,
        display: 'flex',
        flexDirection: 'column',
        gap: 10
    },
    sectionTitle: {
        fontSize: 14,
        alignSelf: 'flex-start'
    }
});

export {
    GlobalStyles
};


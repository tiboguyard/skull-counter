import { SvgXml } from "react-native-svg";
import { SVGProps } from "../../types/svg.type";

export default function HistorySVG(props: SVGProps) {
    const xml = `
        <svg width="800" height="800" viewBox="0 0 66 66" fill="none" xmlns="http://www.w3.org/2000/svg">
            <style type="text/css">
                <![CDATA[.st0{fill:#000000;}]]>
            </style>
            <path fill-rule="evenodd" clip-rule="evenodd" d="M63.25 57.75V63.25H49.5V57.75H63.25ZM46.75 57.75V63.25H41.25V57.75H46.75ZM63.25 49.5V55H49.5V49.5H63.25ZM46.75 49.5V55H41.25V49.5H46.75ZM21.3327 39.1673C24.3186 42.1532 28.4436 44 33 44C34.9285 44 36.7798 43.6691 38.5001 43.0611L38.5002 48.8068C36.7422 49.2593 34.8992 49.5 33 49.5C26.9248 49.5 21.4247 47.0375 17.4435 43.0562L21.3327 39.1673ZM63.25 41.25V46.75H49.5V41.25H63.25ZM46.75 41.25V46.75H41.25V41.25H46.75ZM33 5.5C45.1503 5.5 55 15.3497 55 27.5C55 31.5072 53.9286 35.2642 52.0567 38.5001L45.2983 38.5004C47.9112 35.5812 49.5 31.7261 49.5 27.5C49.5 18.3873 42.1127 11 33 11C23.8873 11 16.5 18.3873 16.5 27.5C16.5 28.0189 16.5239 28.5323 16.5708 29.039L19.25 26.3609L23.1391 30.25L13.75 39.6391L4.3609 30.25L8.24999 26.3609L11.0626 29.1726C11.0211 28.6205 11 28.0627 11 27.5C11 15.3497 20.8497 5.5 33 5.5ZM35.75 13.75V26.015L42.7625 30.7175L39.7375 35.2825L30.25 28.985V13.75H35.75Z"/>
        </svg>
    `;

    return <SvgXml width={props.width} height={props.height} xml={xml} fill={props.fill} pointerEvents="none" />;
}
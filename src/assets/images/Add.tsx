import { SvgXml } from "react-native-svg";
import { SVGProps } from "../../types/svg.type";

export default function AddSVG(props: SVGProps) {
    const xml = `
        <svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
            <style type="text/css">
                <![CDATA[.st0{stroke:#000000;}]]>
            </style>
            <path d="M14 26L14 2" stroke-width="4" stroke-linecap="round"/>
            <path d="M26 14L2 14" stroke-width="4" stroke-linecap="round"/>
        </svg>
    `;

    return <SvgXml width={props.width} height={props.height} xml={xml} stroke={props.stroke} pointerEvents="none" />;
}
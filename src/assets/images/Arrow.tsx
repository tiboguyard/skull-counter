import { SvgXml } from "react-native-svg";
import { SVGProps } from "../../types/svg.type";

function ArrowUpSVG(props: SVGProps) {
    const xml = `
        <svg width="800px" height="800px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M2 16.5833L16.5833 2M16.5833 2H3.45833M16.5833 2V15.125" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
        </svg>
    `;

    return <SvgXml width={props.width} height={props.height} xml={xml} stroke={props.stroke} pointerEvents="none" />;
}


function ArrowDownSVG(props: SVGProps) {
    const xml = `
    <svg width="800px" height="800px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M2 2L16.5833 16.5833M16.5833 16.5833L16.5833 3.45833M16.5833 16.5833L3.45833 16.5833" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
    </svg>
    `;

    return <SvgXml width={props.width} height={props.height} xml={xml} stroke={props.stroke} pointerEvents="none" />;
}

export {
    ArrowDownSVG, ArrowUpSVG
};


import NetInfo from "@react-native-community/netinfo";
import { NavigationProp, useFocusEffect } from "@react-navigation/native";
import { useEffect, useState } from "react";
import { SafeAreaView, Text, TouchableOpacity, View } from "react-native";
import WebView from "react-native-webview";
import AddSVG from "../../assets/images/Add";
import { TabBar } from "../../components/TabBar/TabBar";
import { COLORS_DARK } from "../../constantes/theme/dark.theme";
import { GlobalStyles } from "../../styles/global.style";

export default function GitLab({ navigation }: { navigation: NavigationProp<any> }) {
    /**
     * Live data qui permet d'indiquer si il y a eu un problème au chargement de la page web.
     * @type Hook react
     */
    const [webError, setWebError] = useState<boolean>(false);

    useFocusEffect(() => {
        /**
         * Modification du header de la page gitlab.
         */
        navigation.setOptions({
            statusBarColor: !webError ? '#171321' : COLORS_DARK.black_90,
            header: () => {
                return (
                    <View style={{ paddingHorizontal: 10, paddingVertical: 15, backgroundColor: !webError ? '#171321' : COLORS_DARK.black_90 }} />
                );
            }
        });
    });

    useEffect(() => {
        /**
         * Evénement qui vérifie l'état du réseau de l'appareil.
         * 
         * @event Network
         */
        const netEventConnectionListener = NetInfo.addEventListener((state) => {
            if (state.isConnected) {
                setWebError(false);
            }
        });

        return () => {
            netEventConnectionListener();
        };
    })

    return (
        <>
            <SafeAreaView style={{ flex: 1, backgroundColor: COLORS_DARK.black_90 }}>
                {!webError && (
                    <WebView
                        style={{ flex: 1, backgroundColor: COLORS_DARK.black_90 }}
                        source={{ uri: 'https://gitlab.com/tiboguyard/skull-counter/-/releases' }}
                        onError={() => setWebError(true)}
                    />
                ) || (
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Text>Failed to load web page ! Please check that you have an internet connection !</Text>
                        </View>
                    )
                }
            </SafeAreaView>

            {/* Bouton flottant pour lancer une nouvelle partie */}
            <View style={GlobalStyles.floatCenterContainer}>
                <TouchableOpacity style={{ ...GlobalStyles.roundedButton, width: 90, height: 90, borderWidth: 8, backgroundColor: COLORS_DARK.turquoise, borderColor: COLORS_DARK.black_90 }} onPress={() => navigation.navigate('Party')}>
                    <AddSVG width={30} height={30} fill={COLORS_DARK.black} stroke={COLORS_DARK.black} />
                </TouchableOpacity>
            </View>

            {/* Ajout de la Barre de navigation */}
            <View>
                <TabBar selected='gitlab' onTabPress={(tab) => {
                    if (tab === 'historique') {
                        navigation.navigate('History');
                    } else if (tab === 'gitlab') {
                        navigation.navigate('Gitlab')
                    }
                }} />
            </View>
        </>
    );
}
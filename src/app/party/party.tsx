import AsyncStorage from '@react-native-async-storage/async-storage';
import { NavigationProp, useFocusEffect } from '@react-navigation/native';
import { useCallback, useEffect, useState } from "react";
import { SafeAreaView, ScrollView, Text, TextInput, TouchableOpacity, View } from "react-native";
import Modal from 'react-native-modal';
import AddSVG from '../../assets/images/Add';
import PenSVG from '../../assets/images/Pen';
import RankSVG from '../../assets/images/Rank';
import TrashSVG from '../../assets/images/Trash';
import { CardViewerFromLiveData } from '../../components/Card/Card';
import { GetFormattedDate } from '../../components/Date/Date';
import RankModal from '../../components/Modal/RankModal';
import RoundModal from '../../components/Modal/RoundModal';
import { BidirectionalScrollView } from '../../components/Scroll/BidirectionnalScroll';
import { PartieTabBar } from '../../components/TabBar/TabBar';
import { TableViewer } from '../../components/Table/Table';
import { COLORS_DARK } from '../../constantes/theme/dark.theme';
import { GlobalStyles } from '../../styles/global.style';
import { ModalStyles } from '../../styles/modal.style';
import { NavigationStyle } from '../../styles/navigation.style';
import { HistoryItem } from '../../types/history.type';
import { Ranking } from '../../types/party.type';
import { Player, Round } from "../../types/player.type";

export default function Party({ navigation }: { navigation: NavigationProp<any> }) {
    /**
     * LiveData pour définir le nombre de tour joué.
     * @type Hook react
     */
    const [roundPlayed, setRoundPlayed] = useState<number>(0);

    /**
     * LiveData pour définir les joueurs de la partie.
     * @type Hook react
     */
    const [players, setPlayers] = useState<Player[]>([]);

    /**
     * LiveData pour définir l'heure et la date de début de la partie.
     * @type Hook react
     */
    const [startTimestamp, setStartTimestamp] = useState<string>();

    /**
     * LiveData pour définir les données d'un nouveau joueur.
     * @type Hook react
     */
    const [newPlayerData, setNewPlayerData] = useState({ name: '', score: '' });

    /**
     * LiveData pour définir si la modale d'ajout d'un nouveau joueur est visible.
     * @type Hook react
     */
    const [isNewPlayerModal, setIsNewPlayerModal] = useState<boolean>(false);

    /**
     * LiveData pour définir si la modale d'ajout d'un nouveau joueur doit afficher les joueurs réccurent.
     * @type Hook react
     */
    const [isCasuals, setIsCasuals] = useState<boolean>(false);

    /**
     * LiveData pour définir si la liste des joueurs réccurent.
     * @type Hook react
     */
    const [casuals, setCasuals] = useState<string[] | undefined>(undefined);

    /**
     * LiveData pour définir si la liste des joueurs réccurent sélectionné.
     * @type Hook react
     */
    const [selectedCasuals, setSelectedCasuals] = useState<{ [x: number]: string }>({});

    /**
     * LiveData pour définir si la modale d'affichage du classement des joueurs est visible.
     * @type Hook react
     */
    const [isRankingModal, setIsRankingModal] = useState<boolean>(false);

    /**
     * LiveData pour définir le score des joueurs dans le round actuel avant de les ajouter à la partie.
     * Il permet également la modifiction du round antérieur.
     * @type Hook react
     */
    const [currentRoundScores, setCurrentRoundScores] = useState<Record<number, string>>({});

    /**
     * LiveData pour définir si la modale d'ajout des scores d'un tour est visible.
     * @type Hook react
     */
    const [isRoundSaveModal, setIsRoundSaveModal] = useState<boolean>(false);

    /**
     * LiveData pour définir si le bouton pour enregistrer les scores d'un tour est désactivé.
     * @type Hook react
     */
    const [isSaveRoundDisabled, setIsSaveRoundDisabled] = useState<boolean>(true);


    /**
     * LiveData pour définir si la modale de modification des scores du tour antérieur est visible.
     * @type Hook react
     */
    const [isRoundUpdateModal, setIsRoundUpdateModal] = useState<boolean>(false);

    /**
     * LiveData pour définir l'index du round actuellement modifiable (le dernier round joué).
     * @type Hook react
     */
    const [editableRound, setEditableRound] = useState<number>(-1);

    /**
     * LiveData pour définir le nombre de cartes présentes dans le jeu skull king.
     * @type Hook react
     */
    const [skullKingCards, setSkullKingCards] = useState<number>(74);

    /**
     * Méthode qui permmet de récupérer la gagnant actuel de la partie
     *
     * @return {*}  {(Player | undefined)}
     */
    const getCurrentWinner = (): Player | undefined => {
        if (players.length === 0) return undefined;
        const maxScore = Math.max(...players.map(player => player.score));
        const winners = players.filter(player => player.score === maxScore);
        if (winners.length === 1) {
            return winners[0];
        } else if (winners.length > 1) {
            return {
                name: `${winners.length} winners`,
                score: maxScore,
                scores: []
            } as Player
        } else {
            return undefined;
        }
    };

    /**
     * Méthode qui permet de récupérer le perdant actuel de la partie.
     *
     * @return {*}  {(Player | undefined)}
     */
    const getCurrentLoser = (): Player | undefined => {
        if (players.length === 0) return undefined;
        const lowestScore = Math.min(...players.map(player => player.score));
        const losers = players.filter(player => player.score === lowestScore);
        if (losers.length === 1) {
            return losers[0];
        } else if (losers.length > 1) {
            return {
                name: `${losers.length} losers`,
                score: lowestScore,
                scores: []
            } as Player
        } else {
            return undefined;
        }
    };

    /**
     * Méthode qui permet de récupérer le classement actuel de la partie.
     * Le classement n'est pas trié pour ce faire :
     *  décroissant : Object.keys(playersRanking).map(Number).sort((a, b) => b - a).map(...)
     *  croissant   : Object.keys(playersRanking).map(Number).sort((a, b) => a - b).map(...)
     *
     * @return {*} 
     */
    const getRanking = (): Ranking | undefined => {
        if (players.length === 0) return undefined;
        const rankings: Ranking = players.reduce((acc, player) => {
            const { score, name } = player;
            acc[score] = acc[score] || [];
            acc[score].push(name);
            return acc;
        }, {} as Ranking);
        return rankings;
    };

    /**
     * Méthode qui permet de récupérer les score pour l'ajout d'un nouveau joueurs
     *
     * @param {string} score
     * @return {*} 
     */
    const getNewPlayerScores = (score: string) => {
        const playerScore = score !== '' && roundPlayed > 0 ? parseInt(score) : 0;

        let scores: Round[] = []
        if (roundPlayed > 0) {
            scores = Array.from({ length: roundPlayed - 1 }).map((_, index) => {
                const round: Round = {
                    round: index + 1,
                    scoreOfRound: 0,
                    newScore: 0,
                };
                return round;
            });
            scores = [...scores, { round: roundPlayed, scoreOfRound: playerScore, newScore: playerScore }];
        }

        return { playerScore, scores };
    }

    /**
     * Méthode qui permet d'ajouter un nouveau joueur à la partie.
     *
     */
    const addNewPlayer = ({ name, score }: { name: string, score: string }) => {
        const { playerScore, scores } = getNewPlayerScores(score);

        setPlayers([...players, {
            name: name,
            score: playerScore,
            scores
        }]);
        setNewPlayerData({ name: '', score: '' });
        setIsNewPlayerModal(false);
    };

    /**
     * Méthode qui permet d'ajouter la liste des joueurs réccurents.
     *
     */
    const handleAddCasuals = () => {
        const playersList: Player[] = [...players];
        casuals?.forEach((casual, index) => {
            if (selectedCasuals[index]) {
                const { playerScore, scores } = getNewPlayerScores('0');
                playersList.push({ name: casual, score: playerScore, scores });
            }
        });
        setPlayers(playersList);
        setSelectedCasuals({});
        setIsNewPlayerModal(false);
        setIsCasuals(false);
    };

    /**
     * Méthode qui permet de recalculer les joueurs récurents sélectionnés suite à la suppression d'une personne.
     *
     * @param {number} deletedIndex
     */
    const handleCasualDelete = (deletedIndex: number) => {
        if (Object.keys(selectedCasuals).length > 0) {
            const maxIndex = parseInt(Object.keys(selectedCasuals).reduce((a, b) => selectedCasuals[parseInt(a)] > selectedCasuals[parseInt(b)] ? a : b));
            const minIndex = parseInt(Object.keys(selectedCasuals).reduce((a, b) => selectedCasuals[parseInt(a)] < selectedCasuals[parseInt(b)] ? a : b));

            if (deletedIndex > maxIndex) { return; }
            const newSelectedIndex: { [x: number]: string } = {};
            Object.keys(selectedCasuals).forEach((key) => {
                if (deletedIndex != parseInt(key)) {
                    if (parseInt(key) > deletedIndex) {
                        newSelectedIndex[parseInt(key) - 1] = selectedCasuals[parseInt(key)];
                    } else {
                        newSelectedIndex[parseInt(key)] = selectedCasuals[parseInt(key)];
                    }
                }
            });
            setSelectedCasuals(newSelectedIndex);
        }
    };

    /**
     * Méthode qui permet de gérer l'appui sur un checkbox d'un joueur réccurent.
     *
     * @param {number} index - L'index de la personne dans la liste
     */
    const handleCasualPlayerPress = (index: number) => {
        if (selectedCasuals) {
            if (selectedCasuals[index] == 'selected') {
                const newSelected = Object.fromEntries(
                    Object.entries(selectedCasuals).filter(([key, value]) => key != String(index))
                );
                setSelectedCasuals(newSelected);
            } else {
                setSelectedCasuals({ ...selectedCasuals, [index]: 'selected' });
            }
        }
    };

    /**
     * Méthode qui permet de savoir si le score pour tous les joueurs du round actuel sont défini
     *
     * @param {{ scoreRound: { [x: number]: string } }} { scoreRound }
     * @return {*} 
     */
    const isAllScoreDefined = ({ scoreRound }: { scoreRound: { [x: number]: string } }) => {
        const allScoresDefined = players.every((player, index) => {
            return scoreRound[index] !== undefined && scoreRound[index].trim() !== "";
        });
        return allScoresDefined;
    };

    /**
     * Méthode qui permet de gérer l'ajout du score pour tous les joueurs lors d'un appui sur Win all ou Lose all.
     *
     * @param {{ players: number, score: number }} { players, score }
     */
    const handleAllScoreChange = ({ players, score }: { players: number, score: number }) => {
        const newRoundScore: { [x: number]: string } = {}
        for (let i = 0; i < players; i++) {
            newRoundScore[i] = score.toString();
        }
        setCurrentRoundScores(newRoundScore);
        setIsSaveRoundDisabled(!isAllScoreDefined({ scoreRound: newRoundScore }));
    };

    /**
     * Méthode qui permet de gérer la modifiction du score d'un joueur pour un tour.
     *
     * @param {{ playerIndex: number, newScore: string }} { playerIndex, newScore }
     */
    const handlePlayerScoreChange = ({ playerIndex, newScore }: { playerIndex: number, newScore: string }) => {
        const updatedTempRoundScore = {
            ...currentRoundScores,
            [playerIndex]: newScore,
        };

        setCurrentRoundScores(updatedTempRoundScore);
        setIsSaveRoundDisabled(!isAllScoreDefined({ scoreRound: updatedTempRoundScore }));
    };

    /**
     * Méthode qui permet de gérer les modifications du score pour les joueurs en fonction du round.
     *
     * @param {number} round
     * @return {*}  {Player[]}
     */
    const updatePlayerScore = (round: number): Player[] => {
        const playersList = [...players];
        playersList.forEach((player, index) => {
            if (currentRoundScores[index] !== undefined) {
                const scoreOfUser = parseInt(currentRoundScores[index]);
                const totalScore = round > 0 ? player.scores[round - 1].newScore + scoreOfUser : scoreOfUser;
                player.score = totalScore;
                player.scores[round] = {
                    round: round + 1, // Numéro du round associé au score
                    scoreOfRound: scoreOfUser, // Le score fait par le joueur sur ce round
                    newScore: totalScore, // Le score total des rounds joués par le joueur
                };
            }
        });
        return playersList;
    }

    /**
     * Cette méthode permet de modifier les scores du round antérieur.
     * Elle récupère l'ensemble des scores des joueurs pour le round modifiable (le dernier round joué)
     * et affiche la modale.
     *
     */
    const modifyPreviousRound = () => {
        // Récupération des scores des joueurs pour le round modifiable
        const scoresOfEditableRound = players.map((player, index) => ({
            [index]: player.scores[editableRound].scoreOfRound.toString()
        })).reduce((prev, current) => ({ ...prev, ...current }), {});
        setCurrentRoundScores(scoresOfEditableRound);
        setIsRoundUpdateModal(true);
    };

    /**
     * Méthode qui permet de gérer l'ajout des scores du tour lors de l'appui sur le bouton.
     *
     */
    const handleAddRound = () => {
        setPlayers(updatePlayerScore(roundPlayed));
        setRoundPlayed(roundPlayed + 1);
        setEditableRound(roundPlayed);
        setCurrentRoundScores({});
        setIsRoundSaveModal(false);
        setIsSaveRoundDisabled(true);
    };

    /**
     * Méthode qui permet de gérer la modification des scores d'un tour lors de l'appui sur le bouton.
     *
     */
    const handleUpdateRound = () => {
        setPlayers(updatePlayerScore(editableRound));
        setCurrentRoundScores({});
        setIsRoundUpdateModal(false);
        setIsSaveRoundDisabled(true);
    };

    /**
     * Méthode qui permet de récupérer les joueurs récurrents.
     *
     * @return {*}  {Promise<void>}
     */
    const getCasualPlayers = useCallback(async (): Promise<void> => {
        try {
            const casualStorage = await AsyncStorage.getItem('casual');
            setCasuals(casualStorage ? JSON.parse(casualStorage) as string[] : [] as string[]);
        } catch (error) { setCasuals([] as string[]); }
    }, []);

    /**
     * Méthode qui permet de supprimer un joueur réccurent.
     *
     * @param {number} playerIndex
     */
    const deleteCasual = (playerIndex: number) => {
        const newCasuals = casuals?.filter((casual, index) => index !== playerIndex);
        if (newCasuals == undefined || newCasuals.length == 0) {
            setIsCasuals(false);
        }
        setCasuals(newCasuals);
        if (newCasuals !== undefined) {
            AsyncStorage.setItem('casual', JSON.stringify(Array.from(new Set([...newCasuals])).sort()));
        } else {
            AsyncStorage.setItem('casual', JSON.stringify(Array.from([])));
        }
    }

    /**
     * Méthode qui permet de récupérer les données stockées dans l'historique.
     *
     * @return {*}  {Promise<HistoryItem[]>}
     */
    const getHistoryData = useCallback(async (): Promise<HistoryItem[]> => {
        try {
            const historyStorage = await AsyncStorage.getItem('history');
            return historyStorage ? JSON.parse(historyStorage) as HistoryItem[] : [];
        } catch (error) { return [] as HistoryItem[]; }
    }, []);

    /**
     * Cette méthode permet d'enregistrer la partie.
     *
     * @return {*}  {Promise<void>}
     */
    const saveParty = useCallback(async (): Promise<void> => {
        try {
            const historyData = await getHistoryData();
            const maxId = historyData.length > 0 ? Math.max(...historyData.map(item => item.id)) : 0;
            const newHistoryItem: HistoryItem = {
                id: maxId + 1,
                date: startTimestamp as string,
                winner: getCurrentWinner() as Player,
                loser: getCurrentLoser() as Player,
                rankings: playersRanking ? playersRanking : {},
                players,
                rounds: roundPlayed,
                maxCard: maxCardPerRound
            };
            await AsyncStorage.setItem('history', JSON.stringify([...historyData, newHistoryItem]));
            if (casuals) {
                await AsyncStorage.setItem('casual', JSON.stringify(Array.from(new Set([...casuals, ...players.map(player => player.name)])).sort()));
            } else {
                await AsyncStorage.setItem('casual', JSON.stringify(Array.from(new Set([...players.map(player => player.name)])).sort()));
            }
            navigation.navigate('History');
        } catch (error) { }
    }, [players, roundPlayed]);

    /**
     * Méthode qui permet d'afficher ou masquer la fenêtre contextuelle d'ajout d'une nouvelle personne.
     *
     */
    const toggleNewPlayerModal = () => setIsNewPlayerModal(!isNewPlayerModal);

    /**
     * Méthode qui permet d'afficher ou masque la fenêtre contextuelle d'enregistrement des scores d'un tour.
     *
     */
    const toggleNewRoundModal = () => setIsRoundSaveModal(!isRoundSaveModal);

    /**
     * Méthode qui permet d'afficher ou masque la fenêtre contextuelle d'affichage du classement des joueurs.
     *
     */
    const toggleRankingModal = () => setIsRankingModal(!isRankingModal);

    useEffect(() => {
        setStartTimestamp(GetFormattedDate(new Date()));
        getCasualPlayers();
    }, [])

    useFocusEffect(() => {
        /**
         * Modification du header de la page historique.
         */
        navigation.setOptions({
            header: () => {
                return (
                    <View style={{ ...NavigationStyle.padding, backgroundColor: COLORS_DARK.black_90 }}>
                        <View style={{ ...NavigationStyle.container, backgroundColor: COLORS_DARK.black_80 }}>
                            <View style={NavigationStyle.left}>
                                <Text style={{ ...NavigationStyle.title, color: COLORS_DARK.white }}>GAME</Text>
                            </View>
                            <Text style={{ ...NavigationStyle.subInfo, color: COLORS_DARK.black_40 }}>{startTimestamp}</Text>
                        </View>
                    </View>
                );
            }
        });
    });

    const isRequiredPlayers: boolean = (players.length >= 2); // Bolean qui indique si le nombre minimum de joueurs est atteint
    const maxCardPerRound = Math.floor(skullKingCards / (players.length > 0 ? players.length : 1)); // Nombre de cartes maximal par joueur dans un tour
    const cardsForTheRound = (roundPlayed + 1) > maxCardPerRound ? maxCardPerRound : roundPlayed + 1; // Nombre de cartes joués pour le tour actuel
    const carsForTheEditableRound = (editableRound + 1) > maxCardPerRound ? maxCardPerRound : editableRound + 1; // Nombre de cartes joués sur le tour éditable

    const currentWinner: Player | undefined = getCurrentWinner(); // Vainqueur actuel de la partie
    const currentLoser: Player | undefined = getCurrentLoser(); // Pardant actuel de la partie
    const playersRanking: Ranking | undefined = getRanking(); // Classement actuel des joueurs (non trié)

    const isAddPlayerDisabled: boolean = (newPlayerData.name?.toString().trim() === '' || !newPlayerData.name); // Boolean qui indique si le nom du joueur est vide ou non
    const isAddCasualsDisabled: boolean = Object.keys(selectedCasuals).length <= 0; // Boolean qui indique si l'ajout des joueurs réccurents est activé ou non

    return (
        <>
            <SafeAreaView style={{ flex: 1, paddingBottom: 100, backgroundColor: COLORS_DARK.black_90 }}>
                <View style={GlobalStyles.viewHolder}>
                    <CardViewerFromLiveData loser={currentLoser} winner={currentWinner} players={players} rounds={roundPlayed} maxCard={maxCardPerRound} />
                    {/* Affiche le tableau des noms des joueurs et des scores pour chaque round */}
                    <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', width: '100%' }}>
                        <Text style={{ ...GlobalStyles.sectionTitle, alignSelf: 'center', color: COLORS_DARK.white }}>SCORES TABLE</Text>
                        <View style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'row', gap: 5 }}>
                            <Text style={{ color: COLORS_DARK.white }}>Cards :</Text>
                            <TextInput
                                placeholderTextColor={COLORS_DARK.black_40}
                                style={{ ...ModalStyles.inputWithoutBorder, color: COLORS_DARK.black, backgroundColor: COLORS_DARK.black_40 }}
                                placeholder="Skull cards"
                                maxLength={2}
                                value={isNaN(skullKingCards) ? "" : skullKingCards.toString()}
                                onChangeText={(newScore) => setSkullKingCards(parseInt(newScore))}
                                keyboardType="numeric"
                            />
                        </View>
                    </View>
                    <BidirectionalScrollView>
                        <TableViewer players={players} roundPlayed={roundPlayed} highestScore={currentWinner?.score} lowestScore={currentLoser?.score} />
                    </BidirectionalScrollView>
                </View>
            </SafeAreaView>

            {/* Bouton flottant pour créer un nouveau round */}
            <View style={GlobalStyles.floatCenterContainer}>
                <TouchableOpacity style={{ ...GlobalStyles.roundedButton, width: 90, height: 90, borderWidth: 8, backgroundColor: isRequiredPlayers ? COLORS_DARK.turquoise : COLORS_DARK.turquoise_90, borderColor: COLORS_DARK.black_90 }} onPress={toggleNewRoundModal} disabled={!isRequiredPlayers}>
                    <AddSVG width={30} height={30} fill={isRequiredPlayers ? COLORS_DARK.black : COLORS_DARK.black_80} stroke={isRequiredPlayers ? COLORS_DARK.black : COLORS_DARK.black_80} />
                </TouchableOpacity>
            </View>

            {/* Ajout de la Barre de navigation */}
            <View>
                <PartieTabBar saveDisabled={!(roundPlayed > 0)} onTabPress={(tab) => {
                    if (tab === 'addPlayer') {
                        toggleNewPlayerModal();
                    } else if (tab === 'save') {
                        saveParty();
                    }
                }} />
            </View>

            {/* Bouton flottant pour afficher le classement */}
            {roundPlayed > 0 && <View style={GlobalStyles.floatLeftContainer}>
                <TouchableOpacity style={{ ...GlobalStyles.roundedButton, borderWidth: 3.5, backgroundColor: COLORS_DARK.brown_20, borderColor: COLORS_DARK.brown_80 }} onPress={toggleRankingModal} disabled={!(roundPlayed > 0)}>
                    <RankSVG width={40} height={40} fill={COLORS_DARK.brown_80} stroke={COLORS_DARK.brown_80} />
                </TouchableOpacity>
            </View>}

            {/* Bouton flottant pour modifier le round antérieur */}
            {roundPlayed > 0 && <View style={GlobalStyles.floatRightContainer}>
                <TouchableOpacity style={{ ...GlobalStyles.roundedButton, borderWidth: 3.5, backgroundColor: COLORS_DARK.brown_20, borderColor: COLORS_DARK.brown_80 }} onPress={modifyPreviousRound} disabled={!(roundPlayed > 0)}>
                    <PenSVG width={40} height={40} fill={COLORS_DARK.brown_80} stroke={COLORS_DARK.brown_80} />
                </TouchableOpacity>
            </View>}

            {/* Fenêtre contextuelle pour enregistrer le score d'un tour */}
            <RoundModal
                title='ADD ROUND'
                isVisible={isRoundSaveModal}
                isSaveDisabled={isSaveRoundDisabled}
                players={players}
                scores={currentRoundScores}
                playedCards={cardsForTheRound}
                playedRounds={roundPlayed}
                onSave={handleAddRound}
                onWinAll={(length, score) => handleAllScoreChange({ players: length, score })}
                onLoseAll={(length, score) => handleAllScoreChange({ players: length, score })}
                onCancel={() => { toggleNewRoundModal(); setCurrentRoundScores({}); setIsSaveRoundDisabled(true); }}
                onScoreChanged={handlePlayerScoreChange} />

            {/* Fenêtre contextuelle pour modifier le score du tour antrérieur */}
            <RoundModal
                title='UPDATE ROUND'
                isVisible={isRoundUpdateModal}
                isSaveDisabled={isSaveRoundDisabled}
                players={players}
                scores={currentRoundScores}
                playedCards={carsForTheEditableRound}
                playedRounds={roundPlayed - 1}
                onSave={handleUpdateRound}
                onWinAll={(length, score) => handleAllScoreChange({ players: length, score })}
                onLoseAll={(length, score) => handleAllScoreChange({ players: length, score })}
                onCancel={() => { setIsRoundUpdateModal(false); setCurrentRoundScores({}); setIsSaveRoundDisabled(true); }}
                onScoreChanged={handlePlayerScoreChange} />

            {/* Fenêtre contextuelle pour afficher le classement des joueurs */}
            <RankModal
                isVisible={isRankingModal}
                players={players.length}
                ranking={playersRanking}
                highestScore={currentWinner?.score}
                lowestScore={currentLoser?.score}
                onLeave={toggleRankingModal}
            />

            {/* Fenêtre contextuelle pour ajouter un nouveau joueur */}
            <Modal isVisible={isNewPlayerModal} backdropOpacity={0.9} backdropColor='black' style={{ margin: 0 }}>

                <View style={{ margin: 20 }}>
                    <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'flex-end', gap: 15, marginBottom: 10 }}>
                        <TouchableOpacity onPress={() => setIsCasuals(!isCasuals)} disabled={!(casuals) || !(casuals.length > 0)} style={{ ...GlobalStyles.cornerRoundedButton, borderRadius: 10, borderColor: casuals && casuals.length > 0 ? COLORS_DARK.green : COLORS_DARK.red_20 }}>
                            <Text style={{ color: casuals && casuals.length > 0 ? COLORS_DARK.green : COLORS_DARK.red_20 }}>{'Casuals'}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ ...ModalStyles.container, backgroundColor: COLORS_DARK.black_90, borderColor: COLORS_DARK.black_40 }}>
                        {/* En-tête de la modale */}
                        <View style={ModalStyles.header}>
                            <Text style={{ ...ModalStyles.headerTitle, color: COLORS_DARK.white }}>ADD PLAYERS</Text>
                            <Text style={{ ...ModalStyles.headerSubTitle, color: COLORS_DARK.black_40 }}>Players : {players.length}</Text>
                        </View>

                        {isCasuals && (
                            <>
                                <View style={{ width: '100%', maxHeight: 330 }}>
                                    <ScrollView keyboardShouldPersistTaps={'handled'} showsVerticalScrollIndicator={false}>
                                        {casuals && casuals.map((player, index) => (
                                            <View key={index} style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 15, paddingVertical: 5 }}>
                                                <View style={{ display: 'flex', flexDirection: 'row', gap: 10 }}>
                                                    <TouchableOpacity onPress={() => handleCasualPlayerPress(index)} style={{ width: 25, aspectRatio: 1, borderRadius: 5, borderWidth: 1, borderColor: COLORS_DARK.black_40, }}>
                                                        <View style={{
                                                            flex: 1,
                                                            borderRadius: 3,
                                                            backgroundColor: selectedCasuals && selectedCasuals[index] === 'selected' ? COLORS_DARK.black_40 : 'transparent',
                                                            margin: 4
                                                        }} />
                                                    </TouchableOpacity>
                                                    <Text>{player}</Text>
                                                </View>
                                                <TouchableOpacity onPress={() => { handleCasualDelete(index); deleteCasual(index) }}>
                                                    <TrashSVG width={25} height={25} fill={COLORS_DARK.red_20} stroke={COLORS_DARK.red_20} />
                                                </TouchableOpacity>
                                            </View>
                                        ))}
                                    </ScrollView>
                                </View>

                                {/* Zone d'action */}
                                <View style={ModalStyles.bottom}>
                                    <TouchableOpacity style={{ ...GlobalStyles.cornerRoundedButton, borderColor: !isAddCasualsDisabled ? COLORS_DARK.green : COLORS_DARK.red_20 }} onPress={() => { handleAddCasuals(); toggleNewPlayerModal(); }} disabled={isAddCasualsDisabled}>
                                        <Text style={{ color: !isAddCasualsDisabled ? COLORS_DARK.green : COLORS_DARK.red_20 }}>Add</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ ...GlobalStyles.cornerRoundedButton, borderColor: COLORS_DARK.black_40 }} onPress={toggleNewPlayerModal}>
                                        <Text style={{ color: COLORS_DARK.black_40 }}>Cancel</Text>
                                    </TouchableOpacity>
                                </View>
                            </>
                        ) || (
                                <>
                                    {/* Zon de saisie pour le nom */}
                                    <TextInput
                                        placeholderTextColor={COLORS_DARK.black_40}
                                        style={{ ...ModalStyles.inputBorder, color: COLORS_DARK.white, borderColor: COLORS_DARK.black_40 }}
                                        placeholder="Nom"
                                        value={newPlayerData.name?.toString().trim()}
                                        onChangeText={text => setNewPlayerData({ ...newPlayerData, name: text })}
                                    />

                                    {/* Zone de saisie pour le score à la création */}
                                    <View style={{ display: 'flex', alignItems: 'flex-start', width: '100%' }}>
                                        <Text style={{ color: COLORS_DARK.black_40, fontSize: 10 }}>Optionnal</Text>
                                        <TextInput
                                            placeholderTextColor={COLORS_DARK.black_40}
                                            style={{ ...ModalStyles.inputBorder, color: COLORS_DARK.white, borderColor: COLORS_DARK.black_40 }}
                                            placeholder="Start score"
                                            value={newPlayerData.score?.toString().trim().replace('.', '-')}
                                            onChangeText={text => setNewPlayerData({ ...newPlayerData, score: text })}
                                            keyboardType="numeric"
                                        />
                                    </View>

                                    {/* Zone d'action */}
                                    <View style={ModalStyles.bottom}>
                                        <TouchableOpacity style={{ ...GlobalStyles.cornerRoundedButton, borderColor: !isAddPlayerDisabled ? COLORS_DARK.green : COLORS_DARK.red_20 }} onPress={() => { addNewPlayer({ name: newPlayerData.name, score: newPlayerData.score }); toggleNewPlayerModal(); }} disabled={isAddPlayerDisabled}>
                                            <Text style={{ color: !isAddPlayerDisabled ? COLORS_DARK.green : COLORS_DARK.red_20 }}>Add</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={{ ...GlobalStyles.cornerRoundedButton, borderColor: COLORS_DARK.black_40 }} onPress={toggleNewPlayerModal}>
                                            <Text style={{ color: COLORS_DARK.black_40 }}>Cancel</Text>
                                        </TouchableOpacity>
                                    </View>
                                </>
                            )}
                    </View>
                </View >
            </Modal >
        </>
    );
}
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';

import { COLORS_DARK } from '../constantes/theme/dark.theme';
import { NavigatorParamList } from '../types/navigator.type';
import GitLab from './gitlab/gitlab';
import HistoryItemViewer from './history/[id]';
import History from './history/history';
import Party from './party/party';

const Stack = createNativeStackNavigator<NavigatorParamList>();

export default function App(): JSX.Element {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName='History' screenOptions={{
        headerShown: true,
        navigationBarHidden: true,
        statusBarTranslucent: true,
        statusBarColor: COLORS_DARK.black_90,
      }}>
        <Stack.Screen name='History' component={History} />
        <Stack.Screen name='HistoryID' component={HistoryItemViewer} />
        <Stack.Screen name='Gitlab' component={GitLab} />
        <Stack.Screen name='Party' component={Party} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

import AsyncStorage from '@react-native-async-storage/async-storage';
import { NavigationProp, useFocusEffect } from '@react-navigation/native';
import { useCallback, useState } from 'react';
import { SafeAreaView, ScrollView, Text, TouchableOpacity, View } from 'react-native';
import AddSVG from '../../assets/images/Add';
import TrashSVG from '../../assets/images/Trash';
import { ClickableCardViewer } from '../../components/Card/Card';
import { TabBar } from '../../components/TabBar/TabBar';
import { COLORS_DARK } from '../../constantes/theme/dark.theme';
import { GlobalStyles } from '../../styles/global.style';
import { NavigationStyle } from '../../styles/navigation.style';
import { HistoryItem } from '../../types/history.type';

/**
 * Page historique.
 * Page principale où arrive les personnes au lancement de l'application.
 *
 * @export
 * @param {{ navigation: NavigationProp<any> }} { navigation }
 * @return {*} 
 */
export default function History({ navigation }: { navigation: NavigationProp<any> }) {
    /**
     * LiveData pour récupérer et définir les données de l'historique.
     * @type Hook react
     */
    const [histories, setHistories] = useState<HistoryItem[]>();

    /**
     * Méthode qui permet de récupérer les données de l'historique.
     *
     * @return {*}  {Promise<void>}
     */
    const getHistoryData = useCallback(async () => {
        try {
            const historyStorage = await AsyncStorage.getItem('history');
            if (historyStorage) {
                setHistories(JSON.parse(historyStorage) as HistoryItem[]);
            }
        } catch (error) {
            setHistories([]);
        }
    }, []);

    /**
     * Méthode qui permet de supprimer les données de l'historique.
     *
     * @return {*}  {Promise<void>}
     */
    const resetHistoryData = async (): Promise<void> => {
        try {
            await AsyncStorage.setItem('history', JSON.stringify([]));
            setHistories([]);
        } catch (error) { }
    };

    useFocusEffect(() => {
        getHistoryData();

        /**
         * Modification du header de la page historique.
         */
        navigation.setOptions({
            header: () => {
                return (
                    <View style={{ ...NavigationStyle.padding, backgroundColor: COLORS_DARK.black_90 }}>
                        <View style={{ ...NavigationStyle.container, backgroundColor: COLORS_DARK.black_80 }}>
                            <View style={NavigationStyle.left}>
                                <Text style={{ ...NavigationStyle.title, color: COLORS_DARK.white }}>HISTORY</Text>
                                <Text style={{ ...NavigationStyle.subTitle, color: COLORS_DARK.black_40 }}>Entries: {histories ? histories.length : 0}</Text>
                            </View>
                        </View>
                    </View>
                );
            }
        });
    });

    return (
        <>
            <SafeAreaView style={{ flex: 1, backgroundColor: COLORS_DARK.black_90 }}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{ ...GlobalStyles.viewHolder, flexDirection: 'column-reverse', paddingBottom: 90 }}>
                        {histories && histories.map((history, index) => (
                            <ClickableCardViewer key={index} history={history} displayDate={true} onCardPress={(id) => {
                                navigation.navigate('HistoryID', { id: id });
                            }} />
                        ))}
                    </View>
                </ScrollView>
            </SafeAreaView>

            {/* Bouton flottant pour lancer une nouvelle partie */}
            <View style={GlobalStyles.floatCenterContainer}>
                <TouchableOpacity style={{ ...GlobalStyles.roundedButton, width: 90, height: 90, borderWidth: 8, backgroundColor: COLORS_DARK.turquoise, borderColor: COLORS_DARK.black_90 }} onPress={() => navigation.navigate('Party')}>
                    <AddSVG width={30} height={30} fill={COLORS_DARK.black} stroke={COLORS_DARK.black} />
                </TouchableOpacity>
            </View>

            {/* Ajout de la Barre de navigation */}
            <View>
                <TabBar selected='historique' onTabPress={(tab) => {
                    if (tab === 'historique') {
                        navigation.navigate('History');
                    } else if (tab === 'gitlab') {
                        navigation.navigate('Gitlab')
                    }
                }} />
            </View>

            {/* Bouton flottant de suppression de l'historique */}
            <View style={GlobalStyles.floatRightContainer}>
                <TouchableOpacity style={{ ...GlobalStyles.roundedButton, borderWidth: 3.5, backgroundColor: COLORS_DARK.red_20, borderColor: COLORS_DARK.red_60 }} onPress={resetHistoryData} disabled={!(histories && histories.length > 0)}>
                    <TrashSVG width={40} height={40} fill={COLORS_DARK.red_60} stroke={COLORS_DARK.red_60} />
                </TouchableOpacity>
            </View>
        </>
    );
}
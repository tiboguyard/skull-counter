import AsyncStorage from "@react-native-async-storage/async-storage";
import { NavigationProp, RouteProp } from "@react-navigation/native";
import React, { useEffect, useLayoutEffect, useState } from "react";
import { SafeAreaView, Text, TouchableOpacity, View } from "react-native";
import AddSVG from "../../assets/images/Add";
import RankSVG from "../../assets/images/Rank";
import TrashSVG from "../../assets/images/Trash";
import { CardViewer } from "../../components/Card/Card";
import RankModal from "../../components/Modal/RankModal";
import { BidirectionalScrollView } from "../../components/Scroll/BidirectionnalScroll";
import { TabBar } from "../../components/TabBar/TabBar";
import { TableViewer } from "../../components/Table/Table";
import { COLORS_DARK } from "../../constantes/theme/dark.theme";
import { GlobalStyles } from "../../styles/global.style";
import { NavigationStyle } from "../../styles/navigation.style";
import { HistoryItem } from "../../types/history.type";
import { NavigatorParamList } from "../../types/navigator.type";
import { Ranking } from "../../types/party.type";

export default function HistoryItemViewer({ navigation, route }: { navigation: NavigationProp<any>, route: RouteProp<NavigatorParamList, "HistoryID"> }) {
    const { id } = route.params;

    /**
     * LiveData pour récupérer et définir les données de l'historique à visualiser.
     * @type Hook react
     */
    const [history, setHistory] = useState<HistoryItem>();

    /**
     * LiveData pour sauvegarder les données de tout l'historiqiue.
     * @type Hook react
     */
    const [data, setData] = useState<HistoryItem[]>([]);

    /**
     * LiveData pour définir si la modale d'affichage du classement des joueurs est visible.
     * @type Hook react
     */
    const [isRankingModal, setIsRankingModal] = useState<boolean>(false);

    /**
     * Méthode qui permet de supprimer un historique en fonction de son id.
     *
     * @return {*}  {Promise<void>}
     */
    const deleteHistoryDataFromID = async (): Promise<void> => {
        try {
            if (data.length > 0) {
                const newHistories = data.filter((history) => history.id !== id);
                await AsyncStorage.setItem('history', JSON.stringify(newHistories));
                navigation.navigate('History');
            }
        } catch (error) { }
    };

    /**
     * Méthode qui permet de récupérer le classement de la partie.
     * Le classement n'est pas trié pour ce faire :
     *  décroissant : Object.keys(playersRanking).map(Number).sort((a, b) => b - a).map(...)
     *  croissant   : Object.keys(playersRanking).map(Number).sort((a, b) => a - b).map(...)
     *
     * @return {*} 
     */
    const getRanking = (): Ranking | undefined => {
        if (history == undefined) return undefined;
        if (history.players.length === 0) return undefined;
        const rankings: Ranking = history.players.reduce((acc, player) => {
            const { score, name } = player;
            acc[score] = acc[score] || [];
            acc[score].push(name);
            return acc;
        }, {} as Ranking);
        return rankings;
    };

    /**
     * Méthode qui permet d'afficher ou masque la fenêtre contextuelle d'affichage du classement des joueurs.
     *
     */
    const toggleRankingModal = () => setIsRankingModal(!isRankingModal);

    useEffect(() => {
        /**
         * Méthode qui permet de récupérer les données d'un historique en fonction de son id.
         * 
         * @return {*}  {Promise<void>}
         */
        const getHistoryDataFromID = async () => {
            try {
                const dataHistory = await AsyncStorage.getItem("history");
                if (dataHistory) {
                    const historyData = JSON.parse(dataHistory) as HistoryItem[];
                    setData(historyData);
                    const historyItem = historyData.find(h => h.id === id);
                    if (historyItem) {
                        setHistory(historyItem);
                    }
                }
            } catch (error) { }
        };

        getHistoryDataFromID();
    }, [id]);

    useLayoutEffect(() => {
        /**
         * Modification du header de la page historique:id.
         */
        navigation.setOptions({
            header: () => {
                return (
                    <View style={{ ...NavigationStyle.padding, backgroundColor: COLORS_DARK.black_90 }}>
                        <View style={{ ...NavigationStyle.container, backgroundColor: COLORS_DARK.black_80 }}>
                            <View style={NavigationStyle.left}>
                                <Text style={{ ...NavigationStyle.title, color: COLORS_DARK.white }}>HISTORY</Text>
                                <Text style={{ ...NavigationStyle.subTitle, color: COLORS_DARK.black_40 }}>id : {id}</Text>
                            </View>
                            <Text style={{ ...NavigationStyle.subInfo, color: COLORS_DARK.black_40 }}>{history?.date}</Text>
                        </View>
                    </View>
                );
            }
        });
    }, [history]);

    return (
        <>
            <SafeAreaView style={{ flex: 1, paddingBottom: 100, backgroundColor: COLORS_DARK.black_90 }}>
                <View style={GlobalStyles.viewHolder}>
                    <CardViewer history={history} displayDate={false} />
                    {/* Affiche le tableau des noms des joueurs et des scores pour chaque round */}
                    <Text style={{ ...GlobalStyles.sectionTitle, color: COLORS_DARK.white }}>SCORES TABLE</Text>
                    <BidirectionalScrollView>
                        <TableViewer players={history?.players} roundPlayed={history?.rounds} highestScore={history?.winner.score} lowestScore={history?.loser.score} />
                    </BidirectionalScrollView>
                </View>
            </SafeAreaView>

            {/* Bouton flottant pour lancer une nouvelle partie */}
            <View style={GlobalStyles.floatCenterContainer}>
                <TouchableOpacity style={{ ...GlobalStyles.roundedButton, width: 90, height: 90, borderWidth: 8, backgroundColor: COLORS_DARK.turquoise, borderColor: COLORS_DARK.black_90 }} onPress={() => navigation.navigate('Party')}>
                    <AddSVG width={30} height={30} fill={COLORS_DARK.black} stroke={COLORS_DARK.black} />
                </TouchableOpacity>
            </View>

            {/* Ajout de la Barre de navigation */}
            <View>
                <TabBar selected='' onTabPress={(tab) => {
                    if (tab === 'historique') {
                        navigation.navigate('History');
                    } else if (tab === 'gitlab') {
                        navigation.navigate('Gitlab');
                    }
                }} />
            </View>

            {/* Bouton flottant pour afficher le classement */}
            <View style={GlobalStyles.floatLeftContainer}>
                <TouchableOpacity style={{ ...GlobalStyles.roundedButton, borderWidth: 3.5, backgroundColor: COLORS_DARK.brown_20, borderColor: COLORS_DARK.brown_80 }} onPress={toggleRankingModal}>
                    <RankSVG width={40} height={40} fill={COLORS_DARK.brown_80} stroke={COLORS_DARK.brown_80} />
                </TouchableOpacity>
            </View>

            {/* Bouton flottant de suppression de l'historique */}
            <View style={GlobalStyles.floatRightContainer}>
                <TouchableOpacity style={{ ...GlobalStyles.roundedButton, borderWidth: 3.5, backgroundColor: COLORS_DARK.red_20, borderColor: COLORS_DARK.red_60 }} onPress={deleteHistoryDataFromID}>
                    <TrashSVG width={30} height={30} fill={COLORS_DARK.red_60} stroke={COLORS_DARK.red_60} />
                </TouchableOpacity>
            </View>

            {/* Fenêtre contextuelle pour afficher le classement des joueurs */}
            <RankModal
                isVisible={isRankingModal}
                players={history?.players.length}
                ranking={getRanking()}
                highestScore={history?.winner.score}
                lowestScore={history?.loser.score}
                onLeave={toggleRankingModal}
            />
        </>
    );
}
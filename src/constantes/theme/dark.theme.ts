/**
 * Thème sombre
 */
const COLORS_DARK = {
    black: '#000000',
    black_90: '#121212',
    black_80: '#1D1D1D',
    black_40: '#A8A8A8',
    white: '#FFFFFF',
    turquoise: '#03DAC5',
    turquoise_90: '#026F64',
    purple: '#C38FFF',
    green: '#81CF66',
    red_20: '#CF6679',
    red_60: '#5A212B',
    brown_20: 'rgba(207, 190, 102, 0.45)',
    brown_80: 'rgba(90, 81, 33, 0.85)'
};

export { COLORS_DARK };


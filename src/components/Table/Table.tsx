import React from 'react';
import { Text, View } from 'react-native';
import { COLORS_DARK } from '../../constantes/theme/dark.theme';
import { TableStyle } from '../../styles/table.style';
import { Player } from '../../types/player.type';


type TableViewerProps = {
    players: Player[] | undefined;
    roundPlayed: number | undefined;
    highestScore: number | undefined;
    lowestScore: number | undefined;
}

/**
 * Méthode qui permet de représenter les données d'une partie sous la forme d'un tableau.
 *
 * @param {({ players: Player[], roundPlayed: number, highestScore: number | undefined, lowestScore: number | undefined })} { players, roundPlayed, highestScore, lowestScore }
 * @return {*} 
 */
function TableViewer({ players, roundPlayed, highestScore, lowestScore }: TableViewerProps) {
    if (players?.length === 0) return null;

    /**
     * Méthode qui permet de récupérer l'écart de points entre le joueur et l'actuel premier.
     *
     * @param {number} score
     * @return {*} 
     */
    const getDeltaToCurrentFirstPlayer = (score: number) => {
        return highestScore !== undefined ? highestScore - score : 0;
    }

    /**
     * Méthode qui permet de récupérer l'écart de points entre le joueur et l'actuel dernier.
     *
     * @param {number} score
     * @return {*} 
     */
    const getDeltaToCurrentLastPlayer = (score: number) => {
        return lowestScore !== undefined ? -lowestScore + score : 0;
    }

    return (
        <View style={{ margin: 20 }}>
            <View style={TableStyle.table}>
                {players && players.map((player, index) => (
                    <View key={index} style={TableStyle.row}>
                        <View style={{ display: 'flex', flexDirection: 'row', padding: 7, paddingHorizontal: 9 }}>
                            <Text style={TableStyle.columnName}>{player.name}</Text>
                            {roundPlayed && roundPlayed > 0 && (
                                <View style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                                    <Text style={{ fontSize: 12, fontWeight: 'bold', color: player.score === highestScore ? COLORS_DARK.green : player.score === lowestScore ? COLORS_DARK.red_20 : COLORS_DARK.white }}>
                                        {` (${player.score})`}
                                    </Text>
                                    <View style={{ display: 'flex', flexDirection: 'row' }}>
                                        <Text style={{ fontSize: 10, color: COLORS_DARK.green }}>
                                            {` (${getDeltaToCurrentFirstPlayer(player.score)})`}
                                        </Text>
                                        <Text style={{ fontSize: 10, color: COLORS_DARK.red_20 }}>
                                            {` (${getDeltaToCurrentLastPlayer(player.score)})`}
                                        </Text>
                                    </View>
                                </View>
                            ) || (<></>)}
                        </View>
                        {player.scores && player.scores.map((round, roundIndex) => (
                            <View key={roundIndex} style={index === (players.length - 1) ? TableStyle.lastRowData : TableStyle.rowData}>
                                <Text style={{ fontSize: 17 }}>{round.newScore}</Text>
                                <View style={{ position: 'absolute', left: 3, top: 2, display: 'flex', flexDirection: 'column' }}>
                                    <Text style={{ fontSize: 10 }}>{`[${round.round}]`}</Text>
                                    <Text style={{ fontSize: 10 }}>{`[${round.scoreOfRound}]`}</Text>
                                </View>
                            </View>
                        )) || (<></>)}
                    </View>
                )) || (<></>)}
            </View>
        </View>
    );
}

export { TableViewer };


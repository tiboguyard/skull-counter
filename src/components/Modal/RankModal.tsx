import { ScrollView, Text, TouchableOpacity, View } from "react-native";
import Modal from 'react-native-modal';
import { COLORS_DARK } from "../../constantes/theme/dark.theme";
import { GlobalStyles } from "../../styles/global.style";
import { ModalStyles } from "../../styles/modal.style";
import { Ranking } from "../../types/party.type";

type RankModalProps = {
    isVisible: boolean, // Modale visible ?
    players: number | undefined; // Nombre de joueurs
    ranking: Ranking | undefined; // Le classement des joueurs par leurs scores
    highestScore: number | undefined; // Score le plus élevé
    lowestScore: number | undefined; // Score le plus bas
    onLeave: () => void; // Event de clic sur le bouton 'leave'
}

/**
 * Méthode qui permet d'afficher la modale pour afficher le classement des joueurs.
 *
 * @export
 * @param {RankModalProps} { isVisible, players, ranking, highestScore, lowestScore, onLeave }
 * @return {*} 
 */
export default function RankModal({ isVisible, players, ranking, highestScore, lowestScore, onLeave }: RankModalProps) {
    return (
        <Modal isVisible={isVisible}>
            <View style={{ margin: 20 }}>
                <View style={{ ...ModalStyles.container, backgroundColor: COLORS_DARK.black_90, borderColor: COLORS_DARK.black_40 }}>
                    {/* En-tête de la modale */}
                    <View style={ModalStyles.header}>
                        <Text style={{ ...ModalStyles.headerTitle, color: COLORS_DARK.white }}>RANKING</Text>
                        <Text style={{ ...ModalStyles.headerSubTitle, color: COLORS_DARK.black_40 }}>Players : {players ? players : 0}</Text>
                    </View>

                    {/* Zone d'affichage du classement des joueurs */}
                    <ScrollView showsVerticalScrollIndicator={false} style={{ width: '100%', maxHeight: 330, borderRadius: 10, padding: 15, backgroundColor: COLORS_DARK.black_80 }}>
                        {ranking && Object.keys(ranking).map(Number).sort((a, b) => b - a).map((score, index) => (
                            <View key={index} style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', gap: 5 }}>
                                <Text style={{ fontWeight: 'bold', fontSize: 16, color: highestScore === score ? COLORS_DARK.green : lowestScore === score ? COLORS_DARK.red_20 : COLORS_DARK.black_40 }}>{`${index + 1} - [ ${score} ] :`}</Text>
                                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                                    <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', gap: 10 }}>
                                        {ranking[score].map((player, index2) => (
                                            <Text key={index2} style={{ fontSize: 16, color: highestScore === score ? COLORS_DARK.green : lowestScore === score ? COLORS_DARK.red_20 : COLORS_DARK.black_40 }}>{player}</Text>
                                        ))}
                                    </View>
                                </ScrollView>
                            </View>
                        ))}
                    </ScrollView>

                    {/* Zone d'action */}
                    <View style={ModalStyles.bottom}>
                        <TouchableOpacity style={{ ...GlobalStyles.cornerRoundedButton, borderColor: COLORS_DARK.black_40 }} onPress={onLeave}>
                            <Text style={{ color: COLORS_DARK.black_40 }}>Leave</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </Modal>
    )
}
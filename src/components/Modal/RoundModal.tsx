import { Keyboard, ScrollView, Text, TextInput, TouchableOpacity, TouchableWithoutFeedback, View } from "react-native";
import Modal from 'react-native-modal';
import { COLORS_DARK } from "../../constantes/theme/dark.theme";
import { GlobalStyles } from "../../styles/global.style";
import { ModalStyles } from "../../styles/modal.style";
import { Player } from "../../types/player.type";

type RoundModalProps = {
    title: string;
    isVisible: boolean; // Modale visible ?
    isSaveDisabled: boolean; // Save désactivé ?
    scores: Record<number, string>; // Les scores des personnes pour le tour en cours d'édition
    playedCards: number; // Le nombre de cartes joués sur le tour
    playedRounds: number; // Le nombre de tours joués
    players: Player[]; // La liste des joueurs
    onSave: () => void; // Event de clic sur le bouton de sauvegarde des scores du tour
    onWinAll: (playersLength: number, score: number) => void; // Event de clic sur le bouton 'Win all'
    onLoseAll: (playersLength: number, score: number) => void; // Event de clic sur le bouton 'Lose all'
    onCancel: () => void; // Event de clic sur le bouton cancel du tour
    onScoreChanged: ({ playerIndex, newScore }: { playerIndex: number, newScore: string }) => void; // Event appelé à chaque modification du score d'une personne
}

/**
 * Méthode qui permet d'afficher la modale pour enregistrer les scores d'un round.
 *
 * @export
 * @param {RoundModalProps} { title, isVisible, isSaveDisabled, scores, playedCards, playedRounds, players, onSave, onCancel, onScoreChanged }
 * @return {*} 
 */
export default function RoundModal({ title, isVisible, isSaveDisabled, scores, playedCards, playedRounds, players, onSave, onWinAll, onLoseAll, onCancel, onScoreChanged }: RoundModalProps) {
    return (
        <Modal isVisible={isVisible} backdropOpacity={0.9} backdropColor='black' style={{ margin: 0 }}>
            <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
                <View style={{ margin: 20 }}>
                    <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'flex-end', gap: 15, marginBottom: 10 }}>
                        <TouchableOpacity onPress={() => onWinAll(players.length, playedCards * 10)} style={{ ...GlobalStyles.cornerRoundedButton, borderRadius: 10, borderColor: COLORS_DARK.green }}>
                            <Text style={{ color: COLORS_DARK.green }}>Win all</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => onLoseAll(players.length, playedCards * -10)} style={{ ...GlobalStyles.cornerRoundedButton, borderRadius: 10, borderColor: COLORS_DARK.red_20 }}>
                            <Text style={{ color: COLORS_DARK.red_20 }}>Lose all</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ ...ModalStyles.container, backgroundColor: COLORS_DARK.black_90, borderColor: COLORS_DARK.black_40 }}>
                        {/* En-tête de la modale */}
                        <View style={ModalStyles.header}>
                            <View style={ModalStyles.headerLeft}>
                                <Text style={{ ...ModalStyles.headerTitle, color: COLORS_DARK.white }}>{title}</Text>
                                <Text style={{ ...ModalStyles.headerSubTitle, color: COLORS_DARK.green }}>Cards : {playedCards}</Text>
                            </View>
                            <Text style={{ ...ModalStyles.headerInfo, color: COLORS_DARK.black_40 }}>Round : {playedRounds + 1}</Text>
                        </View>

                        {/* Zone de création des zone de saisies pour les scores du tour */}
                        <ScrollView keyboardShouldPersistTaps={'handled'} showsVerticalScrollIndicator={false} style={{ width: '100%', maxHeight: 330, borderRadius: 10, backgroundColor: COLORS_DARK.black_40 }}>
                            {players.map((player, index) => (
                                <View key={index} style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '100%', paddingHorizontal: 15, paddingVertical: 5 }}>
                                    <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
                                        <Text style={{ fontWeight: 'bold', fontSize: 16, maxWidth: 80, color: COLORS_DARK.black }}>
                                            {player.name.length >= 8 ? `${player.name.substring(0, 4)}... :` : `${player.name} :`}</Text>
                                        <TextInput
                                            placeholderTextColor={COLORS_DARK.black_80}
                                            style={{ ...ModalStyles.inputWithoutBorder, color: COLORS_DARK.black }}
                                            placeholder="Score"
                                            maxLength={4}
                                            value={
                                                scores[index] !== undefined ? scores[index].toString().trim().replace('.', '-') : player.scores[playedRounds]?.toString().trim().replace('.', '-')
                                            }
                                            onChangeText={(newScore) => onScoreChanged({ playerIndex: index, newScore })}
                                            keyboardType="numeric"
                                        />
                                    </View>
                                    <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', gap: 10 }}>
                                        <TouchableOpacity style={{ ...GlobalStyles.cornerRoundedButton, paddingHorizontal: 5, backgroundColor: COLORS_DARK.black_90, borderColor: COLORS_DARK.green }} onPress={() => onScoreChanged({ playerIndex: index, newScore: (playedCards * 10).toString() })}>
                                            <Text style={{ fontWeight: 'bold', color: COLORS_DARK.green }}> Win </Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={{ ...GlobalStyles.cornerRoundedButton, paddingHorizontal: 5, backgroundColor: COLORS_DARK.black_90, borderColor: COLORS_DARK.red_20 }} onPress={() => onScoreChanged({ playerIndex: index, newScore: (playedCards * -10).toString() })}>
                                            <Text style={{ fontWeight: 'bold', color: COLORS_DARK.red_20 }}>Lose</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            ))}
                        </ScrollView>

                        {/* Zone d'action */}
                        <View style={ModalStyles.bottom}>
                            <TouchableOpacity style={{ ...GlobalStyles.cornerRoundedButton, borderColor: !isSaveDisabled ? COLORS_DARK.green : COLORS_DARK.red_20 }} onPress={onSave} disabled={isSaveDisabled}>
                                <Text style={{ color: !isSaveDisabled ? COLORS_DARK.green : COLORS_DARK.red_20 }}>Add</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{ ...GlobalStyles.cornerRoundedButton, borderColor: COLORS_DARK.black_40 }} onPress={onCancel}>
                                <Text style={{ color: COLORS_DARK.black_40 }}>Cancel</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        </Modal>
    );
}
/**
 * Méthode qui permet de récupérer une date formattée
 * @param date 
 * @returns jour/mois/année à heures:minutes
 */
const GetFormattedDate = (date: Date) => {
    return `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()} à ${date.getHours()}:${date.getMinutes().toString().padStart(2, '0')}`;
};

export { GetFormattedDate };


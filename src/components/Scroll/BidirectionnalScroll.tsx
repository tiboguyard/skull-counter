import React, { ReactNode } from 'react';
import { ScrollView } from 'react-native';
import { COLORS_DARK } from '../../constantes/theme/dark.theme';

type BidirectionalScrollViewProps = {
    children: ReactNode;
}

/**
 * Méthode qui permet de créer un zone scrollable dans les deux sens
 *
 * @param {BidirectionalScrollViewProps} props
 * @return {*} 
 */
function BidirectionalScrollView(props: BidirectionalScrollViewProps) {
    return (
        <ScrollView horizontal showsHorizontalScrollIndicator={false} style={{
            flex: 1,
            width: '100%',
            backgroundColor: COLORS_DARK.black_80,
            borderRadius: 15,
            overflow: 'scroll'
        }}>
            <ScrollView centerContent showsVerticalScrollIndicator={false} style={{ height: '100%' }}>
                {props.children}
            </ScrollView>
        </ScrollView>
    )
}

export {
    BidirectionalScrollView
};


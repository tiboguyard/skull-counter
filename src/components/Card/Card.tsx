import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { ArrowDownSVG, ArrowUpSVG } from '../../assets/images/Arrow';
import { COLORS_DARK } from '../../constantes/theme/dark.theme';
import { HistoryItem } from "../../types/history.type";
import { Player } from '../../types/player.type';

type CardViewProps = {
    history: HistoryItem | undefined;
    displayDate: boolean;
}

/**
 * Méthode qui permet de générer la carte d'une historique
 *
 * @param {CardViewProps} { history, displayDate }
 * @return {*} 
 */
function CardView({ history, displayDate }: CardViewProps) {
    return (
        <>
            {/* Zone de gauche */}
            <View style={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-start', gap: 7, height: '100%', width: '50%', padding: 15 }}>
                {displayDate && (<Text style={{ color: COLORS_DARK.black_40 }}>{history?.date}</Text>)}
                {/* Affichage du vainqueur de la partie */}
                <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                    <ArrowUpSVG width={25} height={25} fill={COLORS_DARK.green} stroke={COLORS_DARK.green} />
                    <View style={{ display: 'flex', flexDirection: 'column', alignContent: 'center', justifyContent: 'center' }}>
                        <Text style={{ color: COLORS_DARK.green }}>{history?.winner?.score}</Text>
                        <Text style={{ color: COLORS_DARK.green }}>{history?.winner?.name}</Text>
                    </View>
                </View>
                {/* Affichage du perdant de la partie */}
                <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                    <ArrowDownSVG width={25} height={25} fill={COLORS_DARK.red_20} stroke={COLORS_DARK.red_20} />
                    <View style={{ display: 'flex', flexDirection: 'column', alignContent: 'center', justifyContent: 'center' }}>
                        <Text style={{ color: COLORS_DARK.red_20 }}>{history?.loser?.score}</Text>
                        <Text style={{ color: COLORS_DARK.red_20 }}>{history?.loser?.name}</Text>
                    </View>
                </View>
                <Text style={{ color: COLORS_DARK.black_40 }}>Rounds: {history?.rounds}</Text>
            </View>
            {/* Zone de droite */}
            <View
                style={{
                    display: 'flex',
                    width: '100%',
                    height: '200%',
                    position: 'absolute',
                    justifyContent: 'center',
                    alignItems: 'center',
                    right: '-40%',
                    bottom: '-100%',
                    backgroundColor: COLORS_DARK.black_40,
                    transform: [{ rotate: '25deg' }]
                }}>
                <Text style={{
                    position: 'absolute',
                    left: '3%',
                    bottom: displayDate ? '80%' : '77%',
                    transform: [{ rotate: '-25deg' }],
                    fontSize: 12,
                    color: COLORS_DARK.black
                }}>Maximum cards: {history?.maxCard}</Text>
                <Text style={{
                    position: 'absolute',
                    left: '3%',
                    bottom: displayDate ? '70%' : '65%',
                    transform: [{ rotate: '-25deg' }],
                    fontSize: 12,
                    color: COLORS_DARK.black
                }}>Players: {history?.players?.length}</Text>
            </View>
        </>
    )
}


/**
 * Méthode qui permet de générer la carte d'une historique non cliquable
 *
 * @param {CardViewProps} {history, displayDate}
 * @return {*} 
 */
function CardViewer({ history, displayDate }: CardViewProps) {
    return (
        <View style={{ display: 'flex', flexDirection: 'row', overflow: 'hidden', width: '100%', maxWidth: 364, height: 'auto', borderRadius: 15, backgroundColor: COLORS_DARK.black_80 }}>
            <CardView history={history} displayDate={displayDate} />
        </View>
    );
}

type ClickableCardViewProps = CardViewProps & {
    onCardPress: (id: number) => void;
}

/**
 * Méthode qui permet de générer la carte d'une historique avec un evénement de clique sur la carte
 *
 * @param {ClickableCardViewProps} {history, displayDate, onCardPress}
 * @return {*} 
 */
function ClickableCardViewer({ history, displayDate, onCardPress }: ClickableCardViewProps) {
    return (
        <TouchableOpacity style={{
            display: 'flex', flexDirection: 'row', overflow: 'hidden', width: '100%', maxWidth: 364, height: 'auto', borderRadius: 15, backgroundColor: COLORS_DARK.black_80
        }} onPress={() => { if (history) onCardPress(history.id) }}>
            <CardView history={history} displayDate={displayDate} />
        </TouchableOpacity>
    );
}

type CardViewerFromLiveDataProps = {
    loser: Player | undefined;
    winner: Player | undefined;
    rounds: number;
    maxCard: number;
    players: Player[];
}

/**
 * Méthode qui permet de générer la carte d'une historique à partir de donnée en instantannées
 *
 * @param {CardViewerFromLiveDataProps} { loser, winner, rounds, players, maxCard}
 * @return {*} 
 */
function CardViewerFromLiveData({ loser, winner, rounds, players, maxCard }: CardViewerFromLiveDataProps) {
    const historyItem: HistoryItem = {
        id: -1,
        date: '',
        winner: winner ?? { name: '?', score: 0, scores: [] } as Player,
        loser: loser ?? { name: '?', score: 0, scores: [] } as Player,
        rounds: rounds,
        maxCard: maxCard,
        players: players
    };

    return (
        <CardViewer history={historyItem} displayDate={false} />
    )
}

export {
    CardViewer, CardViewerFromLiveData, ClickableCardViewer
};


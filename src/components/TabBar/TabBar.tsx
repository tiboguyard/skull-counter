import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import AddPeopleSVG from '../../assets/images/AddPeople';
import GitlabSVG from '../../assets/images/GitLab';
import HistorySVG from '../../assets/images/History';
import SaveSVG from '../../assets/images/Save';
import { COLORS_DARK } from '../../constantes/theme/dark.theme';


type TabBarItem = "historique" | "gitlab" | "";
type TabBarProps = {
    selected: TabBarItem;
    onTabPress: (tab: TabBarItem) => void;
}

/**
 * Méthode qui permet de créer une barre de navigation entre les différentes pages (hors partie)
 */
function TabBar({ selected, onTabPress }: TabBarProps) {
    const colorHistorique = selected === 'historique' ? COLORS_DARK.purple : COLORS_DARK.black_40;
    const colorGitlab = selected === 'gitlab' ? COLORS_DARK.purple : COLORS_DARK.black_40;

    return (
        <>
            <View style={{
                width: '100%',
                position: 'absolute',
                elevation: 0,
                height: 80,
                backgroundColor: COLORS_DARK.black_80,
                borderBlockColor: 'transparent',
                borderTopLeftRadius: 50,
                borderTopRightRadius: 50,
                bottom: 0,
                zIndex: 50,
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                gap: 150
            }}>
                <TouchableOpacity style={{ width: 60, height: 60 }} onPress={() => { if (selected !== 'historique') { onTabPress('historique') } }}>
                    {<HistorySVG width={60} height={60} stroke={colorHistorique} fill={colorHistorique} />}
                </TouchableOpacity>
                <TouchableOpacity style={{ width: 60, height: 60, backgroundColor: COLORS_DARK.black_80 }} onPress={() => { if (selected !== 'gitlab') { onTabPress('gitlab') } }}>
                    {<GitlabSVG width={60} height={60} stroke={colorGitlab} fill={colorGitlab} />}
                </TouchableOpacity>
            </View>
        </>
    )
}

type PartieTabBarItem = "addPlayer" | "save";
type PartieTabBarProps = {
    saveDisabled: boolean;
    onTabPress: (tab: PartieTabBarItem) => void;
}
/**
 * Méthode qui permet de créer une barre de navigation entre les différentes pages (en partie)
 */
function PartieTabBar({ saveDisabled, onTabPress }: PartieTabBarProps) {
    const saveColor = saveDisabled ? COLORS_DARK.black_90 : COLORS_DARK.black_40;
    return (
        <>
            <View style={{
                width: '100%',
                position: 'absolute',
                elevation: 0,
                height: 80,
                backgroundColor: COLORS_DARK.black_80,
                borderBlockColor: 'transparent',
                borderTopLeftRadius: 50,
                borderTopRightRadius: 50,
                bottom: 0,
                zIndex: 50,
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                gap: 150
            }}>
                <TouchableOpacity style={{ width: 60, height: 60, backgroundColor: COLORS_DARK.black_80 }} onPress={() => onTabPress('addPlayer')}>
                    {<AddPeopleSVG width={60} height={60} stroke={COLORS_DARK.black_40} fill={COLORS_DARK.black_40} />}
                </TouchableOpacity>
                <TouchableOpacity style={{ width: 60, height: 60, backgroundColor: COLORS_DARK.black_80 }} onPress={() => onTabPress('save')} disabled={saveDisabled}>
                    {<SaveSVG width={60} height={60} stroke={saveColor} fill={saveColor} />}
                </TouchableOpacity>
            </View>
        </>
    )
}

export {
    PartieTabBar, TabBar
};


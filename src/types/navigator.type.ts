/**
 * Type qui décrit les routes aevc leurs parmaètre de recherche dans l'application.
 */
type NavigatorParamList = {
    History: undefined;
    HistoryID: { id: number };
    Gitlab: undefined;
    Party: undefined;
}

export type {
    NavigatorParamList
};


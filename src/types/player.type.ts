/**
 * Type qui définit un joueur
 */
type Player = {
    name: string;
    score: number;
    scores: Round[];
}
/**
 * Type qui décrit un round
 */
type Round = {
    round: number;
    scoreOfRound: number;
    newScore: number;
}

export type {
    Player,
    Round
};


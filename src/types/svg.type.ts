/**
 * Type décrivant les caractéristiques attendus à la création d'un composant SVG
 */
type SVGProps = {
    width: number;
    height: number;
    fill: string;
    stroke: string;
}

export type {
    SVGProps
};


import { Ranking } from "./party.type";
import { Player } from "./player.type";

/**
 * Type qui définit une ligne d'historique
 */
type HistoryItem = {
    id: number;
    date: string;
    loser: Player;
    winner: Player;
    rankings: Ranking;
    rounds: number;
    maxCard: number;
    players: Player[];
}

export type {
    HistoryItem
};


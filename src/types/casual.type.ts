/**
 * Type qui définit les joueurs récurrent
 */
type CasualStorage = {
    casuals: string[];
}

export type {
    CasualStorage
};

